import React, { Component } from 'react'
import {
  Button,
  View,
  ActivityIndicator,
  StatusBar,
  StyleSheet
} from 'react-native'
import nodejs from 'nodejs-mobile-react-native'

import FeedPage from '../pages/feed'
import NetworkPage from '../pages/network'
import ProfilePage from '../pages/profile'
import RecordingPage from '../pages/recording'
import ActionButton from './ActionButton'

function dispatch (action) {
  // an asynchronous action somewhere else, probably involves db
  nodejs.channel.post('action', action)
}

export default class App extends Component {
  constructor () {
    super()

    // NOTE: terrible hand-rolled tabs, but it works
    // in future try e.g. https://www.npmjs.com/package/react-native-tab-view
    this.state = {
      pages: [
        { name: 'feed', page: FeedPage },
        { name: 'network', page: NetworkPage },
        { name: 'profile', page: ProfilePage }
      ],
      currentPage: 0,
      showRecorder: false,
      feedUpdatedAt: null,
      replication: {},
      replicating: false,
      replicatedAt: null
    }

    this.toggleRecorder = this.toggleRecorder.bind(this)
  }

  reducer ({ type, payload }) {
    switch (type) {
      case 'newAudioFile':
        this.setState({
          feedUpdatedAt: Date.now()
        })
        break
      case 'replication':
        this.setState({
          replication: payload
        })
      default:
    }
  }

  componentDidMount () {
    this.listener = nodejs.channel.addListener('mutation', this.reducer, this)
  }

  componentWillMount () {
    nodejs.start('loader.js')
  }

  componentWillUnmount () {
    this.listener.remove() // solves setState on unmounted components!
  }
  componentDidUpdate (prevProps, prevState) {
    if (
      prevState.replication.progress !== this.state.replication.progress &&
      !this.state.replicating
    ) {
      this.setState({ replicating: true })
      setTimeout(() => {
        this.setState({ replicating: false, replicatedAt: Date.now() })
      }, 1000)
    }
  }

  render () {
    // NOTE this is a hack, rework interface when have decent routing
    if (this.state.showRecorder) {
      return (
        <RecordingPage
          onCancel={data => {
            if (data) {
              dispatch({
                type: 'deleteAudioFile',
                payload: data
              })
            }
            this.setState({ showRecorder: false })
          }}
          onPublish={data => {
            dispatch({
              type: 'publishAudioFile',
              payload: data
            })

            this.setState({ showRecorder: false })
          }}
        />
      )
    }

    const Page = this.state.pages[this.state.currentPage].page

    return (
      <View style={styles.container}>
        <StatusBar barStyle='dark-content' />
        <View style={styles.navBar}>
          {this.state.pages.map(({ name, page }, i) => {
            return (
              <Button
                title={name}
                onPress={() => this.setState({ currentPage: i })}
                color={i === this.state.currentPage ? '#f0f' : '#000'}
                key={name}
              />
            )
          })}
        </View>
        <Page {...this.state} />
        {this.state.replicating && (
          <View
            style={{
              backgroundColor: 'black',
              height: 30,
              width: 30,
              position: 'absolute',
              top: '7%',
              left: '45%',
              right: '45%',
              bottom: 0,
              justifyContent: 'center',
              alignItems: 'center',
              zIndex: 99,
              borderRadius: 50
            }}
          >
            <ActivityIndicator
              style={{ zIndex: 99 }}
              size='small'
              color='#00ff00'
            />
          </View>
        )}
        {this.state.currentPage === 0 ? (
          <ActionButton action={() => this.setState({ showRecorder: true })} />
        ) : null}
      </View>
    )
  }

  toggleRecorder () {
    this.setState({ showRecorder: !this.state.showRecorder })
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1
  },
  navBar: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    backgroundColor: '#000'
  }
})
