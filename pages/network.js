import React, { Component, useRef } from 'react'
import { ScrollView, View, Text, Button, StyleSheet } from 'react-native'
import nodejs from 'nodejs-mobile-react-native'

export default class Network extends Component {
  constructor (props) {
    super(props)

    this.state = {
      isLoading: true,
      feedId: 'loading...',
      // mix: this is ugly at the moment. format like :
      // {
      //    multiserverAddress: [
      //      multiserverAddress,
      //      { key, state, ...someTimestamps }
      //    ]
      // }
      connectedPeers: [],
      stagedPeers: []
    }

    this.reducer.bind(this)
  }

  componentDidMount () {
    this.listener = nodejs.channel.addListener('mutation', this.reducer, this)
    dispatch({ type: 'whoami' })
  }

  componentWillUnmount () {
    this.listener.remove() // solves setState on unmounted components!
  }

  reducer ({ type, payload }) {
    switch (type) {
      case 'whoami':
        this.setState({ feedId: payload.feedId })
        break

      case 'connected-peers':
        var connectedPeers = payload.filter(
          ([msa, data]) => data.state === 'connected'
        )

        this.setState({ connectedPeers })
        break

      case 'staged-peers':
        this.setState({ stagedPeers: payload })
        break

      default:
        console.log(type, payload)
    }
  }

  render () {
    return (
      <ScrollView contentInsetAdjustmentBehavior='automatic'>
        <View style={{ padding: 5 }}>
          <View style={styles.section}>
            <Text style={styles.heading}>FeedId: </Text>
            <Text>{this.state.feedId}</Text>
          </View>

          <View style={styles.section}>
            <Text style={styles.heading}>connected peers</Text>
            {this.state.connectedPeers.map(ConnectedPeer)}
          </View>

          <View style={styles.section}>
            <Text style={styles.heading}>potential peers</Text>
            {this.state.stagedPeers.map(StagedPeer)}
          </View>
        </View>
      </ScrollView>
    )
  }

}

// NOTE these are function components, they're just functions
function ConnectedPeer (data) {
  const [address, { key, name, state }] = data

  return (
    <View style={styles.stagedPeer} key={address + state}>
      <Text>{key.slice(0, 10)}...</Text>
      <Text> {name || '??'} </Text>
      <Text> {state} </Text>
    </View>
  )
}
function StagedPeer (data) {
  const [address, { key, pool, type }] = data
  return (
    <View style={styles.stagedPeer} key={key}>
      <Text>{key.slice(0, 10)}... </Text>
      <Text>from {pool} </Text>
      <Text>of type {type} </Text>
      <Button title='follow' onPress={doFollow(key)} />
    </View>
  )
}

function doFollow (feedId) {
  const action = {
    type: 'doFollow',
    payload: feedId
  }
  return () => dispatch(action)
}

// function doConnect (address) {
//   const action = {
//     type: 'connect',
//     payload: address
//   }
//   return () => dispatch(action)
// }

// function getFollow ({ source, dest }) {
//   const action = {
//     type: 'getFollow',
//     payload: { source, dest }
//   }
//   return () => dispatch(action)
// }

function dispatch (action) {
  // an asynchronous action somewhere else, probably involves db
  nodejs.channel.post('action', action)
}

const styles = StyleSheet.create({
  section: {
    padding: 5,
    marginBottom: 10
  },
  heading: {
    fontWeight: 'bold'
  },
  stagedPeer: {
    flex: 1,
    flexDirection: 'row'
  },
  textInput: {
    fontSize: 20,
    borderWidth: 1,
    borderBottomColor: '#000',
    marginBottom: 10
  }
})
